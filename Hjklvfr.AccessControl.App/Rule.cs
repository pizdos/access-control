﻿using Hjklvfr.AccessControl.App.Objects;
using Hjklvfr.AccessControl.App.Subjects;

namespace Hjklvfr.AccessControl.App
{
    public class Rule
    {
        public readonly User Subject;
        public readonly Action Action;
        public readonly File Object;

        public Rule(User subject, Action action, File o)
        {
            Subject = subject;
            Action = action;
            Object = o;
        }
    }
}