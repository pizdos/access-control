﻿namespace Hjklvfr.AccessControl.App.Objects
{
    public class File
    {
        public readonly string Path;
        public readonly int SizeBytes;
        public readonly int Level;

        public File(string path, int sizeBytes, int level)
        {
            Path = path;
            SizeBytes = sizeBytes;
            Level = level;
        }
    }
}