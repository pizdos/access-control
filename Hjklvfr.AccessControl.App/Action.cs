﻿namespace Hjklvfr.AccessControl.App
{
    public enum Action
    {
        Read,
        Write,
        Change,
        Delete,
        List,
        Execute,
    }
}